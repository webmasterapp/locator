<?php /** @noinspection PhpMultipleClassesDeclarationsInOneFile */

declare(strict_types=1);
namespace MasterApp\Locator;
use Contributte\Translation\Translator;
use Exception;
use Nette\Http\IRequest;

/**
 * Class Locator
 * @package App\Models\System
 */
class Locator {

    private Locales $locales;

    /** @var string  */
    private const ipHeaderName = 'cf-connecting-ip';

    /** @var string  */
    private const countryHeaderName = 'cf-ipcountry';

    public function __construct(Locales $locales) {
        $this->locales = $locales;
    }

    /**
     * @param IRequest $request
     * @return string
     * @throws LocatorResolveAddressException
     */
    public static function getIpAddressString(IRequest $request) : string {

        // CF
        $headers = $request->getHeaders();
        $remoteAddress = $headers[self::ipHeaderName] ?? null;
        if (!empty($remoteAddress)) { return $remoteAddress; }

        // PHP
        $remoteAddress = $request->getRemoteAddress();
        if (!empty($remoteAddress)) { return $remoteAddress; }
        throw new LocatorResolveAddressException('Impossible to resolve IP address');
    }

    /**
     * @return Locales
     */
    public function getLocales() : Locales {
        return $this->locales;
    }

    /**
     * @param IRequest $request
     * @param bool $useAcceptLanguage
     * @return Locale
     */
    public function getCountryLocaleFromRequestWithFallBack(IRequest $request, $useAcceptLanguage=true) : Locale {

        $countryCode = $this->getCountryCodeFromRequest($request, $useAcceptLanguage);
        $countryLocale = $countryCode !== null ? $this->locales->getCountryLocaleByCountryCode($countryCode) : null;
        return $countryLocale ?? $this->locales->returnDefaultLocale();
    }

    /**
     * @param IRequest $request
     * @param bool $useAcceptLanguage
     * @return string|null
     */
    public function getCountryCodeFromRequest(IRequest $request, $useAcceptLanguage=true) :? string {

        // When we have locale like en_US, parse country and we are complete
        $localeStringFromBrowser = null;
        if ($useAcceptLanguage && $request->getHeader('Accept-language') !== null) {
            $localeStringFromBrowser = locale_accept_from_http($request->getHeader('Accept-language'));
            if (is_string($localeStringFromBrowser) && !empty($localeStringFromBrowser) && str_contains($localeStringFromBrowser, '_')) {
                $explodedLocaleString = explode('_', $localeStringFromBrowser, 2);
                if (isset($explodedLocaleString[1]) && strlen($explodedLocaleString[1]) === 2) {
                    return strtolower($explodedLocaleString[1]);
                }
            }
        }

        // Get country code from IP
        $countryCodeByIP = $countryCodeByIP = $this->getCountryCodeByIp($request);

        // When we have locale like en
        if (is_string($localeStringFromBrowser) && strlen($localeStringFromBrowser) === 2) {

            // We have jut one country for the locale -> we know the country
            $localesCount = 0;
            $localeFound = null;
            foreach ($this->locales->getAllSupportedLocales() as $currentLocale) {
                if ($currentLocale->languageCode === strtolower($localeStringFromBrowser)) {
                    $localeFound = $currentLocale;
                    ++$localesCount;
                }
            }
            if ($localesCount === 1 && $localeFound !== null) {
                return $localeFound->countryCode;
            }

            // When we have locale like en
            if ($countryCodeByIP !== null) {

                // We are sure because we have also same country
                foreach ($this->locales->getAllSupportedLocales() as $currentLocale) {
                    if ($currentLocale->languageCode === strtolower($localeStringFromBrowser) && $currentLocale->countryCode === strtolower($countryCodeByIP)) {
                        return $currentLocale->countryCode;
                    }
                }
            }
        }

        return $countryCodeByIP;
    }

    /**
     * @param IRequest $request
     * @param bool $useAcceptLanguage
     * @return string|null
     */
    public function getLanguageCodeFromRequest(IRequest $request, $useAcceptLanguage=true) :? string {

        // When we have locale like en_US, parse country and we are complete
        $localeStringFromBrowser = null;
        if ($useAcceptLanguage && $request->getHeader('Accept-language') !== null) {
            $localeStringFromBrowser = locale_accept_from_http($request->getHeader('Accept-language'));
            if (is_string($localeStringFromBrowser) && !empty($localeStringFromBrowser)) {
                if (strlen($localeStringFromBrowser) === 2) {
                    return strtolower($localeStringFromBrowser);
                }
                if (str_contains($localeStringFromBrowser, '_')) {
                    $explodedLocaleString = explode('_', $localeStringFromBrowser, 2);
                    if (isset($explodedLocaleString[0]) && strlen($explodedLocaleString[0]) === 2) {
                        return strtolower($explodedLocaleString[0]);
                    }
                }
            }
        }

        $countryCodeByIP = $this->getCountryCodeByIp($request);
        $localeByIP = $countryCodeByIP !== null ? $this->getLocales()->getCountryLocaleByCountryCode($countryCodeByIP) : null;
        return $localeByIP !== null ? $localeByIP->languageCode : null;
    }

    /**
     * @param IRequest $request
     * @return string|null
     */
    private function getCountryCodeByIp(IRequest $request) :? string {

        // CloudFlare found -> parse the headers
        $countryCodeByIP = null;
        $headers = $request->getHeaders();
        $countryCode = $headers[self::countryHeaderName] ?? null;
        if (!empty($countryCode) && $countryCode !== 'XX' && $countryCode !== 'T1') {
            $countryCodeByIP =  $countryCode;
        }
        return $countryCodeByIP;
    }

    /**
     * @param IRequest $request
     * @param Translator|null $translator
     * @param string|null $locale
     * @param bool $useAcceptLanguage
     * @return Locale
     */
    public function processLocale(IRequest $request, ?Translator $translator=null, ?string $locale=null, $useAcceptLanguage=true) : Locale {

        $localeObject = null;
        if ($locale === null || $locale === 'auto') {
            $localeObject = $this->getCountryLocaleFromRequestWithFallBack($request, $useAcceptLanguage);
        }
        else {
            $localeObjectFromCountry = $this->locales->getCountryLocaleByCountryCode($locale);
            $localeObjectDefault = $this->locales->returnDefaultLocale();
            $localeObject = $localeObjectFromCountry ?? $localeObjectDefault;
        }

        if ($translator !== null) { $translator->setLocale($localeObject->translation); }
        return $localeObject;
    }
}

/**
 * Class LocatorResolveAddressException
 * @package MasterApp\Locator
 */
class LocatorResolveAddressException extends Exception {}