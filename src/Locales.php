<?php
/** @noinspection PhpMultipleClassesDeclarationsInOneFile */

namespace MasterApp\Locator;


/**
 * Class Locales
 * @package App\Config
 */
class Locales  {

    private array $allImplementedLocales = [];

    public function returnDefaultLocale() : Locale {

        $locale = new Locale();
        $locale->countryCode = 'us';
        $locale->translation = 'us';
        $locale->languageCode = 'en';
        $locale->languageWithCountryCode = 'en-US';
        return $locale;
    }

    /**
     * @param string $countryCode
     * @param string $languageCode
     * @param string $translation
     */
    public function addImplementedLocale(string $countryCode, string $languageCode, string $translation) : void {

        $localeObject = new Locale();
        $localeObject->countryCode = strtolower($countryCode);
        $localeObject->translation = strtolower($translation);
        $localeObject->languageCode = strtolower($languageCode);
        $localeObject->languageWithCountryCode = strtolower($languageCode).'-'.strtolower($countryCode);
        $this->allImplementedLocales[] = $localeObject;
    }

    public function getArrayIndexedByCountryCodes() : array {

        $output = [];
        foreach ($this->allImplementedLocales as $locale) { $output[$locale->countryCode] = $locale; }
        return $output;
    }

    public function getCountryLocaleByCountryCode(string $countryCode) : ?Locale {

        $codeSmall = strtolower($countryCode);
        foreach ($this->allImplementedLocales as $locale) {
            if ($locale->countryCode === $codeSmall) {
                return $locale;
            }
        }
        return null;
    }

    public function getCountryLocaleByLanguageWithCountryCode(string $languageWithCountryCode) : ?Locale {

        foreach ($this->allImplementedLocales as $locale) {
            if ($locale->languageWithCountryCode === $languageWithCountryCode) {
                return $locale;
            }
        }
        return null;
    }

    public function getAllSupportedLocales() : array {
        return $this->allImplementedLocales;
    }

    public function getAllSupportedLanguagesWithCountryCodes() : array {
        return array_unique(array_map(static function ($locale) { return $locale->languageWithCountryCode; }, $this->allImplementedLocales), SORT_STRING);
    }

    public function getAllSupportedCountries() : array {
        return array_unique(array_map(static function ($locale) { return $locale->countryCode; }, $this->allImplementedLocales), SORT_STRING);
    }

    public function getAllSupportedLanguages() : array {
        return array_unique(array_map(static function ($locale) { return $locale->languageCode; }, $this->allImplementedLocales), SORT_STRING);
    }

    public function getAllSupportedTranslations() : array {
        return array_unique(array_map(static function ($locale) { return $locale->translation; }, $this->allImplementedLocales), SORT_STRING);
    }
}

/**
 * Class Locale
 * @package App\Config
 */
class Locale {

    public string $languageCode;

    public string $countryCode;

    public string $translation;

    public string $languageWithCountryCode;

}