<?php

namespace MasterApp\Locator\DI;
use MasterApp\Locator\Locales;
use MasterApp\Locator\Locator;
use RuntimeException;
use Nette\DI\CompilerExtension;

/**
 * Class LocatorExtension
 * @package MasterApp\ExceptionLogger\DI
 */
class LocatorExtension extends CompilerExtension {

    public function loadConfiguration() : void {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('locator'))->setType(Locator::class);

        $localesItem = $builder->addDefinition($this->prefix('locales'));
        $localesItem->setType(Locales::class);

        if (!isset($config['locales'])) { throw new RuntimeException('LocatorExtension missing locales option in config'); }

        foreach ($config['locales'] as $locale) {

            if (!isset($locale['translation'])) { throw new RuntimeException('LocatorExtension missing translation option in config locale'); }
            if (!isset($locale['languageCode'])) { throw new RuntimeException('LocatorExtension missing languageCode option in config locale'); }
            if (!isset($locale['countryCode'])) { throw new RuntimeException('LocatorExtension missing countryCode option in config locale'); }
            $localesItem->addSetup('addImplementedLocale', [$locale['countryCode'], $locale['languageCode'], $locale['translation']]);
        }
    }
}
